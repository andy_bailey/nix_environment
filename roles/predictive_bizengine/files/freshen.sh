#!/bin/bash

ssh root@dev.dataengine.predictivescience.com 'cd /var/www/html/bizengine/ && . build/ENV/bin/activate && DJANGO_SETTINGS_MODULE="bizengine.dev" bizengine/manage.py dumpdata --natural-foreign --natural -e clients.permission --traceback' > seed_data.json
