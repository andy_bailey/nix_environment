* export DJANGO_SETTINGS_MODULE="bizengine.dev"
* ./manage.py dumpdata --natural-foreign --natural -e clients.permission --traceback > bizengine_seed_data.json
* unset DJANGO_SETTINGS_MODULE
