if test -n "$HOME"; then
    # Set up the per-user profile.

    export NIX_USER_PROFILE_DIR=/nix/var/nix/profiles/per-user/$USER

    mkdir -m 0755 -p $NIX_USER_PROFILE_DIR
    if test "$(stat --printf '%u' $NIX_USER_PROFILE_DIR)" != "$(id -u)"; then
        echo "WARNING: bad ownership on $NIX_USER_PROFILE_DIR" >&2
    fi

    # Create ~/.nix-profile if needed

    if ! test -L $HOME/.nix-profile; then
        echo "creating $HOME/.nix-profile" >&2
        if test "$USER" != root; then
            ln -s $NIX_USER_PROFILE_DIR/profile $HOME/.nix-profile
        else
            # Root installs in the system-wide profile by default.
            ln -s /nix/var/nix/profiles/default $HOME/.nix-profile
        fi
    fi

    # Put stuff in the global profile and the user profile in the PATH

    export NIX_PROFILES="/nix/var/nix/profiles/default $HOME/.nix-profile"

    for i in $NIX_PROFILES; do
        export PATH=$i/bin:$PATH
    done

    # Subscribe the root user to the NixOS channel by default.
    if [ "$USER" = root -a ! -e $HOME/.nix-channels ]; then
        echo "http://nixos.org/channels/nixpkgs-unstable nixpkgs" > $HOME/.nix-channels
    fi

    # Create the per-user garbage collector roots directory.
    NIX_USER_GCROOTS_DIR=/nix/var/nix/gcroots/per-user/$USER
    mkdir -m 0755 -p $NIX_USER_GCROOTS_DIR
    if test "$(stat --printf '%u' $NIX_USER_GCROOTS_DIR)" != "$(id -u)"; then
        echo "WARNING: bad ownership on $NIX_USER_GCROOTS_DIR" >&2
    fi

    # Set up a default Nix expression from which to install stuff.
    if [ ! -e $HOME/.nix-defexpr -o -L $HOME/.nix-defexpr ]; then
        echo "creating $HOME/.nix-defexpr" >&2
        rm -f $HOME/.nix-defexpr
        mkdir $HOME/.nix-defexpr
        if [ "$USER" != root ]; then
            ln -s /nix/var/nix/profiles/per-user/root/channels $HOME/.nix-defexpr/channels_root
        fi
    fi

    # Set up secure multi-user builds: non-root users build through the
    # Nix daemon.
    if test "$USER" != root; then
        export NIX_REMOTE=daemon
    else
        export NIX_REMOTE=
    fi
fi
