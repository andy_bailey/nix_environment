#!/bin/sh
export RABBITMQDIR=/home/vagrant/.rabbitmq/3.4.3/var/lib/rabbitmq
export RABBITMQ_NODE_IP_ADDRESS=192.168.33.14
export RABBITMQ_SERVER_START_ARGS="-rabbit error_logger tty -rabbit sasl_error_logger false"
#export SYS_PREFIX=""
export RABBITMQ_ENABLED_PLUGINS_FILE=${RABBITMQDIR}/enabled_plugins
rabbitmq-server
