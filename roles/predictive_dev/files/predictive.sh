function upsearch () {
  slashes=${PWD//[^\/]/}
  directory="$PWD"
  for (( n=${#slashes}; n>0; --n ))
  do
    test -e "$directory/$1" && echo "$directory/$1" && return
    directory="$directory/.."
  done
}

function activate () {
  DIR=$(upsearch build)
  if [ -d "$DIR" ]; then
      if [ -f "${DIR}/${HOSTNAME}/bin/activate" ]; then
          . "${DIR}/${HOSTNAME}/bin/activate"
          return
      else
        . "${DIR}/ENV/bin/activate"
        return
      fi
  fi

  DIR=$(upsearch _env)
  if [ -d "$DIR" ]; then
      . ${DIR}/bin/activate
      return
  fi
  echo "Activating $(readlink -m ${DIR})"
  echo "Couldn't find 'build' or '_env' here or anywhere above"
}

alias sdps="sudo docker ps"
export REPODIR=/sandbox
alias start_elastic="$REPODIR/toys/scripts/start_docker_elasticsearch.sh"
alias stop_elastic="$REPODIR/toys/scripts/stop_docker_elasticsearch.sh"
alias start_cassandra="$REPODIR/toys/scripts/start_docker_cassandra.sh"
alias stop_cassandra="$REPODIR/toys/scripts/stop_docker_cassandra.sh"
alias start_nginx="$REPODIR/toys/scripts/start_docker_nginx.sh"
alias stop_nginx="$REPODIR/toys/scripts/stop_docker_nginx.sh"
alias start_rabbit="$REPODIR/toys/scripts/start_docker_rabbitmq.sh"
alias stop_rabbit="$REPODIR/toys/scripts/stop_docker_rabbitmq.sh"
alias start_postgres="$REPODIR/toys/scripts/start_docker_postgres.sh"
alias stop_postgres="$REPODIR/toys/scripts/stop_docker_postgres.sh"
alias start_clarabridge="$REPODIR/toys/scripts/start_docker_clarabridge.sh"
alias stop_clarabridge="$REPODIR/toys/scripts/stop_docker_clarabridge.sh"
alias c="clear"
alias gs="git status"
alias gl="git log"
alias gp="git pull"
alias gdm="git diff master --stat"
alias gd="git diff"
alias gdw="git diff --color-words"
alias gb="git branch"
alias l="ll"
alias activate2=". ./_env/bin/activate"
alias clean_merged="git branch --merged master | grep -v master | xargs -n 1 git branch -d"
alias clean_merged_remote="git branch --merged master | grep -v master | xargs -n 1 git push origin --delete"
alias start_bizengine="$REPODIR/toys/scripts/start_bizengine.sh"
alias start_bizengine_min="$REPODIR/bizengine/build/ENV/bin/python $REPODIR/bizengine/bizengine/manage.py runserver 0.0.0.0:7743"
alias start_celery="$REPODIR/toys/scripts/start_celery.sh"
alias start_celery_min="$REPODIR/dh_celery/build/ENV/bin/celery worker -A dh_celery -b amqp://guest:guest@localhost:5672// -c 10 -l INFO"
alias start_connector="$REPODIR/toys/scripts/start_connector.sh"
alias start_vital_signs="$REPODIR/toys/scripts/start_vital_signs.sh"
# RAB: this modifies the users personal .gitconfig, might be worth
# thinking about a method for having vagrant or puppet configure the users
# dotfiles individually
git config --global color.ui true
export PS1="[V3] \w$ "
