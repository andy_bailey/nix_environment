# Instructions

    brew install ansible
    vagrant up

# Explanation

I started with Phil and Brians toys/vagrant directory, and gradually
extended it little by little.

I had four distinct issues that I was looking to solve:

* While developing, it was often tricky when I needed to make a
  change to the configuration of one of the infrastructure components
  like Postgresql or Cassandra, such as adjusting log severity, or
  substituting a different version of Elasticsearch. Although running
  our apps in containers is handy, running the backend services in
  containers is kind of a pain. The ability to "reset" them to
  a vanilla state is handy, though, but we can do that without the container.

* I found in many cases that suspending the Macbook, then having it
  unsuspend and DHCP an IP on a different subnet, processes running
  inside Docker containers listening for TCP connections would often
  become unreachable for causes that were not easily debugged, and
  would clear if the container were restarted.

* Some of the default provisioning was disruptive to my normal
  workflow (a large number of two letter aliases for example), and I
  would like to have a lot of my own "stuff", dotfiles, .emacs.d, etc
  inside the VM, but I wouldn't want to muck up $HOME/vagrant for
  everyone at PS. So I created roles for "my stuff", and other
  developers will be able to add theirs as well, and then customize
  the VM a little by adding their roles to playbook.yml.

* Finally, I'd run into trouble with old versions of few things- the
  latest version of git packages for Centos 6.5 (which is EOL'd) is
  1.7.x. I could build new versions of these thing from source as part
  of the provisioning, but the web of dependencies gets unmanageable quickly.

  To solve this, So I modified the provisioning step to install nix on
  the machine, and I install the development tools via nix. This way I
  can have as many different versions of those things installed as I
  like, and I can roll forward and back transactionally to test
  whether a new version of, say, python, breaks anything.

I used Ansible since I already had "roles" created for a lot of the
stuff I wanted, and it's very easy for Python hackers to extend
Ansible. I think this should make it a little easier to
customize the Toys environment, since you can use one of the small
roles as a template.
