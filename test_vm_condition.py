#!/usr/bin/env python
import subprocess
import sys
import json
import logging
import StringIO
import erlcfgparse
import os
import socket
import nose
import psycopg2
import yaml
import urllib2
from cassandra.cluster import Cluster

logger = logging.getLogger(__name__)
sh = logging.StreamHandler(sys.stdout)
logger.setLevel(logging.DEBUG)
sh.setLevel(logging.DEBUG)
logger.addHandler(sh)

from erlcfgparse import ErlconfigHandler

class RabbitmqctlStatusHandler(ErlconfigHandler):
    def __init__(self, data={}):
        self.data = data
        self.ctx = []
        self.curelem = None

    def start_list(self, value):
        self.ctx.append([])
        self.curelem = self.ctx[-1]

    def start_tuple(self, value):
        self.ctx.append([])
        self.curelem = self.ctx[-1]

    def handle_separator(self, value):
        pass

    def end_list(self, value):
        pass

    def end_tuple(self, value):
        if len(self.curelem) == 0:
            return

        self.data[self.curelem.pop(0)] = self.curelem

    def handle_identifier(self, value):
        self.curelem.append(value)

    def handle_quoted_string(self, value):
        self.curelem.append(value[1:-1])

    def handle_binary(self, value):
        self.curelem.append(value[3:-3])

    def handle_newline(self, value):
        pass

def check_socket(inet_addr, port):
    s = socket.socket()
    try:
        s.connect((inet_addr, port))
        return True
    except Exception, e:
        print e
    return False

def supervisorctl(command, facility=""):
    try:
        j = subprocess.check_output(["supervisorctl",
                                     command,
                                     facility])
        return j.rstrip().split()[1]
    except Exception, x:
        sys.stderr.write(str(x))
        return None

def docker_exec(image, cmd):
    try:
        j = subprocess.check_output(["/usr/bin/sudo",
                                     "/usr/bin/docker",
                                     "exec",
                                     image] + cmd.split())
        return j
    except:
        return None

def rabbitmqctl(command):
    return subprocess.check_output("rabbitmqctl %s" % command, shell=True)

def docker_container_status(name):
    try:
        j = subprocess.check_output(["/usr/bin/sudo", "/usr/bin/docker", "inspect", name])
        [t] = json.loads(j)
        return t
    except:
        return {}

def docker_container_running(name):
    return docker_container_status(name).get('State',{}).get('Running') == True

################################################################################
# begin tests
################################################################################

def test_rabbitmq_running():
    """Verify that the the rabbitmq supervisor process is marked running"""
    assert supervisorctl('status', 'rabbitmq') == 'RUNNING'

def test_rabbitmq_node_responds():
    """Verify that the the rabbitmqctl can communicate with the rabbitmq node"""

    result = StringIO.StringIO(rabbitmqctl("status"))
    # the first line of rabbitmqctl is not part of the returned erlang term literl
    result.readline()
    erlp = erlcfgparse.ErlconfigParser()
    cfg = {}
    handler = RabbitmqctlStatusHandler(cfg)
    erlp.parse(result, handler)
    assert cfg.has_key("pid")

def test_rabbitmq_amqp_port():
    """Verify that the the amqp TCP listener is up"""
    assert check_socket("localhost", 5672)
    
def test_postgresql_supervisor_running():
    """Verify that the the postgresql docker container is running"""
    assert supervisorctl("status", "postgresql") == 'RUNNING'

def test_postgres_port():
    """Verify that the the postgresql TCP listener is up"""
    assert check_socket("localhost", 5432)

def test_postgres_domsock():
    """Verify that we can """
    conn = psycopg2.connect("dbname=bizengine user=biz password=biz host=/var/run/postgresql")
 
def test_cassandra_supervisor_running():
    """Verify that the the cassandra docker container is running"""
    assert supervisorctl("status", "cassandra") == 'RUNNING'

def test_cassandra_client():
    cluster = Cluster(['127.0.0.1'])
    session = cluster.connect(keyspace='dev_dataengine')
    
def test_elasticsearch_supervisor_running():
    """
    Verify that the the elasticsearch supervisor is running
    If this test fails, say start_elastic
    """
    assert supervisorctl("status", "elasticsearch") == 'RUNNING'

def test_elasticsearch_port_9200():
    """test that elasticsearch server is responding on tcp 9200"""
    assert check_socket("localhost", 9200)

def test_elasticsearch_port_9300():
    """test that elasticsearch server is responding on tcp 9300"""
    assert check_socket("localhost", 9300)

def test_cassandra_port():
    """test that supervisords inet_http server is responding"""
    assert check_socket("localhost", 9042)

def test_supervisord_pidfile_exists():
    assert os.path.exists("/tmp/supervisord.pid")

def test_supervisord_running():
    with open("/tmp/supervisord.pid", 'r') as pidfile:
        pid = int(pidfile.readline().strip())
    assert pid
    # requires privilege
    # os.kill(pid, 0)

def test_supervisord_inet_http():
    """test that supervisords inet_http server is responding"""
    assert check_socket("localhost", 9001)

def test_supervisor_bizengine():
    """test that bizengine is running"""
    assert supervisorctl('status', 'bizengine') == 'RUNNING'

def test_bizengine_port():
    """Verify that the bizengine django instance port is up"""
    assert check_socket("localhost", 7743)

def test_memcached_supervisor_running():
    assert supervisorctl('status', 'memcached') == 'RUNNING'

def test_memcached_port_11211():
    """test that memcached server is responding on tcp 11211"""
    assert check_socket("localhost", 11211)

def test_celery_running():
    assert supervisorctl('status', 'celery') == 'RUNNING'

#    with open("/tmp/celery.pid", 'r') as pidfile:
#        pid = int(pidfile.readline().strip())
#    assert pid
    # requires privilege
    # os.kill(pid, 0)

def test_nginx_running():
    with open("/var/run/nginx.pid", 'r') as pidfile:
        pid = int(pidfile.readline().strip())
    assert pid
    # requires privilege
    # os.kill(pid, 0)

def test_nginx_80():
    """test that nginx server is responding on tcp 80"""
    assert check_socket("localhost", 80)

def test_supervisor_connector():
    """test that connector is running"""
    assert supervisorctl('status', 'connector') == 'RUNNING'

def test_connector_3034():
    """test whether connector is listening on 3034 if
    test_supervisor_connector passes but this test fails it is likely
    that the connector application has failed to start but Tornado is
    still running.
    """
    assert check_socket("localhost", 3034)

#def test_connector_logfile_present():
#    """test whether the connector logfile is present"""
#    assert os.path.exists("/var/log/connector.log")

# def test_connector_tornado_errors():
#     """test whether /var/log/connector.log contains obvious errors
#     Although Tornado will still be running, the connector bits of it may
#     crash at startup, for example if bizengine is unreachable. Connector
#     will require a restart in this scenario.
#     """
#     patterns = [
#         '--- no python application found',
#         'ERROR:'
#         ]
#     matches = []
#     with open("/var/log/connector.log", 'r') as logfh:
#         for line in logfh:
#             matches += [ line for p in patterns if line.startswith(p) ]
#     assert matches == []

def test_can_get_token():
    cfg = yaml.load(open("/sandbox/connector/config/vm.yaml", "r"))
    payload = json.dumps({"username": cfg['web']['username'], "password": cfg['web']['password']})
    req = urllib2.Request(cfg['web']['token_url'], payload, {'Content-Type': 'application/json'})
    response = urllib2.urlopen(req)
    result = response.read()
    token = json.loads(result)['token']

def main():
    result = nose.run()
    
if __name__ == "__main__":
    main()
